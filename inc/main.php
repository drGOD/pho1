<?php
use Rain\Tpl;
function resl($str){
	$str=str_replace(Array("http://","https://","vk.com","/","http:","https:"), "", $str);
	return $str;
}
$app->map("/groups",function() use($data){
	$tpl = new Tpl;
	include('cfg.php');
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$result = $db->query("SELECT t.group_id, CASE WHEN g.group_screen_name IS NULL THEN CONCAT('club',t.group_id) ELSE  g.group_screen_name END as name, t.cnt, g.last_update FROM groups g RIGHT OUTER JOIN (SELECT group_id, COUNT(*) as cnt FROM main GROUP BY group_id) t ON t.group_id = g.group_id ORDER BY rand()");
	//echo $db->error;
	$i=0;
	$list=Array();
	$code="";$summ=0;
	while ($row = $result->fetch_row()) {
		$code.='<tr>';
		$code.=sprintf('<td><a target="_blank" href="https://vk.com/%s" id="gr%s">%s</a></td>',  $row[1],$i, $row[1]);
		$code.=sprintf('<td>%d</td>', $row[2]);
		$summ+=$row[2];
		$list[]=resl(str_replace("club","",$row[1]));
		$code.="</tr>";
		$i++;
	}
	$tpl->assign("code",$code);
	$tpl->assign("summ",$summ);
	$tpl->assign("list",implode(",",$list));
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("tmp","groups");
	$tpl->draw( "layout" );
})->via("POST","GET");

$app->map("/add_group",function() use($data){
	$tpl = new Tpl;
	include('cfg.php');
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	if(isset($_POST["link"]) AND $_POST["link"]!==""){
		$stmt = $db->prepare("INSERT IGNORE INTO sugg(link) VALUES(?)");
		//echo $db->error;
		$stmt->bind_param('s', $_POST["link"]);
		$tpl->assign("ok",1);
		$stmt->execute();
		$stmt->close();
	}
	$stmt = $db->query("SELECT * FROM sugg WHERE `done`=0 ORDER by id ASC");
	
	while ($row = $stmt->fetch_row()) {
		$list[]=htmlspecialchars($row[1]);
	}
	$tpl->assign("list",$list);
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("tmp","add_group");
	$tpl->draw( "layout" );
})->via("POST","GET");

$app->map("/invites",function() use($data){
	$tpl = new Tpl;
	include('cfg.php');
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	if(isset($_POST["email"]) AND $_POST["email"]!==""){
		$stmt = $db->prepare("INSERT IGNORE INTO invites(email) VALUES(?)");
		$stmt->bind_param('s', $_POST["email"]);
		$tpl->assign("ok",1);
		$stmt->execute();
		$stmt->close();
	}
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("tmp","invites");
	$tpl->draw( "layout" );
})->via("POST","GET");

$app->get('/count',function() use($data){
	include('cfg.php');
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
	$result = $db->query("SELECT COUNT(*) FROM main WHERE 1");
	$result=$result->fetch_row();
	echo $result[0];
	die;
});

?>